# *Candida albicans* SC 5314

## Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJNA10701](https://www.ebi.ac.uk/ena/browser/view/PRJNA10701)
* **Assembly accession**: [GCA_000182965.3](https://www.ebi.ac.uk/ena/browser/view/GCA_000182965.3)
* **Original submitter**: Candida Genome Database

## Assembly overview

* **Assembly level**: Chromosome
* **Assembly name**: Assembly 22 (haplotype A)
* **Assembly length**: 14,282,666
* **#Chromosomes**: 8
* **Mitochondiral**: Yes
* **N50 (L50)**: 2,231,883 (3)

## Annotation overview

* **Original annotator**: Candida Genome Database
* **CDS count**: 6030
* **Pseudogene count**: 18
* **tRNA count**: 126
* **rRNA count**: 4
* **Mobile element count**: 12
