# Change log

This document summerizes the cumulative changes in genome annoations
since the initial annotation retieved from the EBI.

## v1.1 (2020-09-29)

### Deleted

* locus CAALFM_C109110WA deleted: fusioned with CAALFM_C109100WA.
* locus CAALFM_C204450WA deleted: fusioned with CAALFM_C204440WA.
* locus CAALFM_C204780WA deleted: fusioned with CAALFM_C204770WA.
* locus CAALFM_C306390WA deleted: spurious overlap, partial copy of an existing gene.
* locus CAALFM_C400920CA deleted: fusioned with CAALFM_C400910CA.
* locus CAALFM_C401710CA deleted: spurious locus that is included in a larger locus.
* locus CAALFM_C502510CA deleted: fusioned with CAALFM_C502500CA.
* locus CAALFM_C702430CA deleted: integrated into a larger locus CAALFM_C702420CA corresponding to a Gag/Pol TE.
* locus CAALFM_C702610CA deleted: integrated into a larger locus CAALFM_C702600CA corresponding to a Gag/Pol TE.

### Edited

* CDS CAALFM_C100270WA turned into pseudo.
* CDS CAALFM_C100280WA turned into pseudo.
* CDS CAALFM_C106690WA turned into pseudo.
* CDS CAALFM_C109100WA edited coordinates and fusion with CAALFM_C109110WA.
* CDS CAALFM_C113370WA turned into pseudo.
* CDS CAALFM_C113590WA turned into pseudo.
* CDS CAALFM_C114630CA turned into pseudo.
* CDS CAALFM_C201880CA turned into pseudo.
* CDS CAALFM_C202670CA edited coordinates: new methionine.
* CDS CAALFM_C204100WA edited coordinates: new methionine.
* CDS CAALFM_C204440WA edited coordinates and fusion with CAALFM_C204450WA.
* CDS CAALFM_C204770WA edited coordinates and fusion with CAALFM_C204770WA.
* CDS CAALFM_C204880CA turned into pseudo.
* CDS CAALFM_C206610CA turned into pseudo (but should be deleted: juste an extremely degerated relic of ET).
* CDS CAALFM_C208320CA turned into pseudo.
* CDS CAALFM_C208380CA edited coordinates: extended to stop codon.
* CDS CAALFM_C301440CA turned into pseudo.
* CDS CAALFM_C301450CA turned into pseudo.
* CDS CAALFM_C306360CA turned into pseudo.
* CDS CAALFM_C400910CA edited coordinates and fusion with CAALFM_C400920CA.
* CDS CAALFM_C401720CA edited coordinates: extended to stop codon.
* CDS CAALFM_C405800CA edited coordinates: extended to stop codon.
* CDS CAALFM_C406460CA edited coordinates: extended to stop codon.
* CDS CAALFM_C406950WA edited coordinates: extended to stop codon.
* CDS CAALFM_C406950WA edited coordinates: extended to stop codon.
* CDS CAALFM_C407250CA edited coordinates: extended to stop codon.
* CDS CAALFM_C407260WA turned into pseudo.
* CDS CAALFM_C501730WA edited coordinates: extended to stop codon.
* CDS CAALFM_C501990WA edited coordinates: reduced to a valid methionine.
* CDS CAALFM_C502360CA turned into pseudo.
* CDS CAALFM_C502500CA edited coordinates and fusion with CAALFM_C502510CA.
* CDS CAALFM_C503210CA turned into pseudo.
* CDS CAALFM_C504720CA turned into pseudo.
* CDS CAALFM_C600010WA turned into pseudo.
* CDS CAALFM_C600970CA edited coordinates: extended to stop codon and methionine.
* CDS CAALFM_C601500CA edited coordinates: extended to stop codon.
* CDS CAALFM_C601550CA edited coordinates: extended to stop codon.
* CDS CAALFM_C601920CA edited coordinates: extended to stop codon.
* CDS CAALFM_C602120WA edited coordinates: extended to stop codon.
* CDS CAALFM_C700010CA turned into pseudo.
* CDS CAALFM_C702420CA completely refactored: Isolated locus grouped to form a complete Gag/Pol mobile element.
* CDS CAALFM_C702600CA completely refactored: Isolated locus grouped to form a complete Gag/Pol mobile element.
* CDS CAALFM_CR01550CA edited coordinates: extended to stop codon.
* CDS CAALFM_CR02340WA edited coordinates: extended to stop codon.

### Fixed

* Coordinates of mRNA CAALFM_C104290CA (bad format).
* Coordinates of mRNA CAALFM_C601700WA (bad format).
* Coordinates of mRNA CAALFM_CR08360CA (bad format).

## v1.0 (2020-09-29)

### Added

* The 8 annotated chromosomes of Candida albicans SC 5314 (source EBI, [GCA_000182965.3](https://www.ebi.ac.uk/ena/browser/view/GCA_000182965.3))
